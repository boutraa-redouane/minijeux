package serveur;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;

public class Serv {

	private static OutputStream os;
	private static InputStream is;
	protected static int res = 0;


	public static void main(String[] args) throws Exception {

		ServerSocket serv = new ServerSocket(8080);
		int again = 4;
		res = (new Random()).nextInt(10);
		
		System.out.println("en attente de connexion\n");
		Socket soc = serv.accept();
		os = soc.getOutputStream();
		is = soc.getInputStream();
		
		os.write(("connexion reussi, quel est votre nom ?").getBytes());
		System.out.println(Serv.res);
		String name = "" + is.read((new byte[5]));
		
		while (again > 0) {
			os.write(("entrer un nbr entre 0 et 100 : \n").getBytes());
			
			int maRes = is.read() - 48;
			System.out.println(maRes);

			if( maRes == Serv.res) {
				os.write((name + "a gagne\n").getBytes());
				break;
			}else if(maRes > Serv.res) {
				os.write(("-\n").getBytes());
			}else if(maRes < Serv.res) {
				os.write(("+\n").getBytes());
			}
			again--;
			System.out.println(again);
		}
		if(again == 0) {
			os.write((name +" a perdu, ").getBytes());
		}
		os.write(("au revoir\n").getBytes());
		soc.close();
	}
}
